# How to install a nice font for your terminal

Just run the font.sh script by
```
./font.sh
```
When the script get finished open your terminal ---> preferences ---> custom font and select any font at your own discretion
